#include "mform.h"
#include "test.h" // the resources

#define minVersion       sysMakeROMVersion(5,0,0,sysROMStageRelease,0)
#define PalmVersion1     sysMakeROMVersion(1,0,0,sysROMStageRelease,0)

FileRef log;

CharPtr scale0;
CharPtr scale1;
CharPtr scale2;
CharPtr scale3;
CharPtr scale4;
CharPtr scale5;
CharPtr scale6;
CharPtr scale7;
CharPtr scale8;
CharPtr scale9;

void scaleList(int i, CharPtr scale)
{
    switch(i)
    {
     default: return;
     case 0:  scale0 = scale; break;
     case 1:  scale1 = scale; break;
     case 2:  scale2 = scale; break;
     case 3:  scale3 = scale; break;
     case 4:  scale4 = scale; break;
     case 5:  scale5 = scale; break;
     case 6:  scale6 = scale; break;
     case 7:  scale7 = scale; break;
     case 8:  scale8 = scale; break;
     case 9:  scale9 = scale; break;
    }
}

CharPtr scaleList(int i)
{
    switch(i)
    {
     default: return 0;
     case 0:  return scale0;
     case 1:  return scale1;
     case 2:  return scale2;
     case 3:  return scale3;
     case 4:  return scale4;
     case 5:  return scale5;
     case 6:  return scale6;
     case 7:  return scale7;
     case 8:  return scale8;
     case 9:  return scale9;
    }
}

static Err IsCompat(UInt32 requiredVersion, UInt16 launchFlags) {
	UInt32 romVersion;

	// See if we're on in minimum required version of the ROM or later.
	FtrGet(sysFtrCreator, sysFtrNumROMVersion, &romVersion);
	if (romVersion < requiredVersion)
		{
		if ((launchFlags & (sysAppLaunchFlagNewGlobals | sysAppLaunchFlagUIApp)) ==
			(sysAppLaunchFlagNewGlobals | sysAppLaunchFlagUIApp))
			{
			FrmAlert (Incompat);
		
			// Palm OS 1.0 will continuously relaunch this app unless we switch to 
			// another safe one.
			if (romVersion <= PalmVersion1)
				{
				AppLaunchWithCommand(sysFileCDefaultApp, sysAppLaunchCmdNormalLaunch, NULL);
				}
			}
		
		return sysErrRomIncompatible;
		}

	return errNone;
}

void *FrmGetObjectPtrFromID(FormPtr frmP, UInt16 id)
{
    return FrmGetObjectPtr(frmP,FrmGetObjectIndex(frmP,id));
}

static Boolean appHandleEvent(EventPtr eventP)
{
  UInt16 formId;
  FormPtr frmP;

  if (eventP->eType == frmLoadEvent) {
    // Load the form resource.
    formId = eventP->data.frmLoad.formID;
    frmP = FrmInitForm(formId);
    FrmSetActiveForm(frmP);

    // Set the event handler for the form.  The handler of the currently
    // active form is called by FrmHandleEvent each time is receives an
    // event.
    switch (formId) {
    case MAIN:
     FrmSetEventHandler(frmP, mainFrmEvtHnd);
     break;
    case SCALE:
     FrmSetEventHandler(frmP, scaleFrmEvtHnd);
     break;
    default:
//     ErrFatalDisplay("Invalid Form Load Event");
     break;
    }
    return true;
  }

  return false;
}

static void appEventLoop(void)
{
  UInt16 error;
  EventType event;

  do {
    EvtGetEvent(&event, evtWaitForever);

    if (! SysHandleEvent(&event))
      if (! MenuHandleEvent(0, &event, &error))
        if (! appHandleEvent(&event)) {
            FrmDispatchEvent(&event);
        }
  } while (event.eType != appStopEvent);
}

static Err openLog( void )
{
    Boolean getLabel = false;
    Boolean found = false;
    static const Char *dbname = "/fl/feellog.log";
    Char *volLabel = (Char*)MemPtrNew(255);
    MemSet(volLabel,255,0);
    Char *curVolLabel = (Char*)MemPtrNew(255);
    MemSet(curVolLabel,255,0);
    Err err = GetVLabelFromADB(volLabel);
    if(err != errNone) getLabel = true;
    VolumeInfoType vi;
    UInt16 vRefn;
    UInt32 vItr = vfsIteratorStart;
    while(vItr != vfsIteratorStop)
    {
	err = VFSVolumeEnumerate(&vRefn,&vItr);
	if(err != errNone) {
	    MemPtrFree(volLabel);
	    MemPtrFree(curVolLabel);
	    return err;
	}
        VFSVolumeInfo(vRefn,&vi);
        if(vi.attributes & vfsVolumeAttrReadOnly) continue;
	VFSVolumeGetLabel(vRefn,curVolLabel,255);
	if(getLabel) {
	    StoreLabel(curVolLabel);
	    StrCopy(volLabel, curVolLabel);
	}
	if(StrCompare(curVolLabel,volLabel) == 0) { found = true; break; }
    }
    MemPtrFree(volLabel);
    MemPtrFree(curVolLabel);
    if(!found) return errNone+1;
    err = VFSDirCreate(vRefn,"/fl");
    if(err != errNone && err != vfsErrFileAlreadyExists) return err;
    err = VFSFileOpen(vRefn, dbname,vfsModeCreate|vfsModeWrite,&log);
    if(err != errNone) return err;
    UInt32 fsize;
    VFSFileSize(log,&fsize);
    if(fsize > (128*1024)) VFSFileResize(log,0);
    err = VFSFileSeek(log,vfsOriginEnd,0);
    return errNone;
}

Err init(void) {
  Err err = openLog();
  for(int i = 0; i<10;i++)
  {
    scaleList(i,0);
  }

  loadScale();
  return errNone;
}

static void end_app(void) {
  for(int i =0;i<10;i++)
    if(scaleList(i) != 0) MemPtrFree(scaleList(i));

  VFSFileClose(log);
  FrmCloseAllForms ();
}

UInt32 StartF(UInt16 cmd, MemPtr cmdPBP, UInt16 launchFlag) {
  Err error;
  error = IsCompat(minVersion,launchFlag);
  if(error) return (error);
  switch(cmd) {
  case sysAppLaunchCmdNormalLaunch:
   error = init();
   if(error) { end_app(); return (error); }
   FrmGotoForm(MAIN);
   appEventLoop();
   end_app();
   break;
  default:
   break;
  }
  return errNone;
}

UInt32 PilotMain(UInt16 cmd, MemPtr cmdPBP, UInt16 launchFlag) {
  return StartF(cmd,cmdPBP,launchFlag);
}
