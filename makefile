CC=m68k-palmos-gcc -palmos5r4
CXX=m68k-palmos-g++ -palmos5r4
RC=./rc
LD=build-prc
CTOR=F33l
DEBUG=-O2

RES=formz.ro
OBJ=main.o mform.o sform.o

OUT=feellog.prc
NAME='Feel Logger'

.SUFFIXES: .rcp .ro

.rcp.ro: $*.d
	@echo "RC  $*.ro"
	@$(RC) $<

.cc.o: $*.d
	@echo "CC  $*.o"
	@$(CXX) -c $(DEBUG) $<

all: $(OUT) clean_tmp

$(OUT): $(RES) $(OBJ) makefile
	@echo Creating $(OUT)...
	@$(CXX) $(DEBUG) -o tmp $(OBJ)
	@$(LD) -o $(OUT) -n $(NAME) -c $(CTOR) $(RES) tmp

clean: clean_tmp
	@echo "RM  $(OUT)"
	@rm -f $(OUT)

clean_tmp:
	@echo "Removing temporary files..."
	@rm -f test.h *o *~ tmp
