#include "mform.h"
#include "test.h"

static void draw_board(FormPtr frmP) {
// custom Drawings are placed here.
}

DmOpenRef OpenDB(UInt16 mode) {
 static const Char *adbName = "Feel Logger.db";
 LocalID adbId = DmFindDatabase(0,adbName);
 if(adbId == 0) {
  DmCreateDatabase(0, adbName, 'F33l', 'ladb', false);
  adbId = DmFindDatabase(0,adbName);
 }
 return DmOpenDatabase(0,adbId,mode);
}

Err GetVLabelFromADB(Char *lab) {
 DmOpenRef adb = OpenDB(dmModeReadOnly);
 if(DmNumRecords(adb) == 0) return errNone+1;
 MemHandle recH = DmGetRecord(adb,0);
 Char *rec = (Char*)MemHandleLock(recH);
 StrCopy(lab,rec);
 MemHandleUnlock(recH);
 DmReleaseRecord(adb,0,false);
 DmCloseDatabase(adb);
 return errNone;
}

void StoreLabel(const Char *lab) {
 DmOpenRef adb = OpenDB(dmModeReadWrite);
 UInt16 idx = 0;
 MemHandle recH;
 if(DmNumRecords(adb) == 0) { recH = DmNewRecord(adb,&idx,255); DmReleaseRecord(adb,idx,false); }
 recH = DmGetRecord(adb,idx);
 if(recH == 0) {
  FormPtr f = FrmGetActiveForm();
  FrmAlert(Incompat);
  FrmUpdateForm( FrmGetFormId( f ), 0 );
  return;
 }
 MemPtr rec = MemHandleLock(recH);
 DmSet(rec,0,255,0);
 DmStrCopy(rec,0,lab);
 MemHandleUnlock(recH);
 DmReleaseRecord(adb,idx,false);
 DmCloseDatabase(adb);
}

static Err WriteData(const void *data) {
    Err err;
    Boolean getLabel = false;
    Boolean found = false;
    static const Char *dbname = "/fl/felog.fdb";
    Char *volLabel = (Char*)MemPtrNew(255);
    MemSet(volLabel,255,0);
    Char *curVolLabel = (Char*)MemPtrNew(255);
    MemSet(curVolLabel,255,0);
    err = GetVLabelFromADB(volLabel);
    if(err != errNone) getLabel = true;
    VolumeInfoType vi;
    UInt16 vRefn;
    UInt32 vItr = vfsIteratorStart;
    while(vItr != vfsIteratorStop)
    {
	err = VFSVolumeEnumerate(&vRefn,&vItr);
	if(err != errNone) {
	    MemPtrFree(volLabel);
	    MemPtrFree(curVolLabel);
	    return err;
	}
        VFSVolumeInfo(vRefn,&vi);
        if(vi.attributes & vfsVolumeAttrReadOnly) continue;
	VFSVolumeGetLabel(vRefn,curVolLabel,255);
	if(getLabel) {
	    StoreLabel(curVolLabel);
	    StrCopy(volLabel, curVolLabel);
	}
	if(StrCompare(curVolLabel,volLabel) == 0) { found = true; break; }
    }
    MemPtrFree(volLabel);
    MemPtrFree(curVolLabel);
    if(!found) return errNone+1;
    err = VFSDirCreate(vRefn,"/fl");
    if(err != errNone && err != vfsErrFileAlreadyExists) return err;
    FileRef db;
    err = VFSFileOpen(vRefn, dbname,vfsModeCreate|vfsModeWrite,&db);
    if(err != errNone) return err;
    err = VFSFileSeek(db,vfsOriginEnd,0);
    if(err != errNone && err != vfsErrFileEOF) return err;
    err = VFSFileWrite(db,StrLen((const Char*)data),data,NULL);
    VFSFileClose(db);
    return err;
}

Char *fileGetLine(FileRef file) {
  if(VFSFileEOF(file) != errNone) return 0;
  UInt32 startPos, lineSize=0;
  VFSFileTell(file,&startPos);
  Char c=0;
  while(c != '\n')
  {
    if(VFSFileRead(file,1,&c,0) != errNone) { c = '\n'; }
    lineSize++;
  }
  UInt32 memSize = ((lineSize%4 == 0) ? 4 : lineSize%4) + lineSize;
  CharPtr line = (CharPtr)MemPtrNew(memSize);
  MemSet(line,memSize,0);
  VFSFileSeek(file,vfsOriginBeginning,startPos);
  VFSFileRead(file,lineSize,line,0);
  line[lineSize-1]=0;
  return line;
}

Err loadScale( void ) {
    Err err;
    Boolean getLabel = false;
    Boolean found = false;
    static const Char *dbname = "/fl/scale.fdb";
    Char *volLabel = (Char*)MemPtrNew(255);
    MemSet(volLabel,255,0);
    Char *curVolLabel = (Char*)MemPtrNew(255);
    MemSet(curVolLabel,255,0);
    err = GetVLabelFromADB(volLabel);
    if(err != errNone) getLabel = true;
    VolumeInfoType vi;
    UInt16 vRefn;
    UInt32 vItr = vfsIteratorStart;
    while(vItr != vfsIteratorStop)
    {
	err = VFSVolumeEnumerate(&vRefn,&vItr);
	if(err != errNone) {
	    MemPtrFree(volLabel);
	    MemPtrFree(curVolLabel);
	    return err;
	}
        VFSVolumeInfo(vRefn,&vi);
        if(vi.attributes & vfsVolumeAttrReadOnly) continue;
	VFSVolumeGetLabel(vRefn,curVolLabel,255);
	if(getLabel) {
	    StoreLabel(curVolLabel);
	    StrCopy(volLabel, curVolLabel);
	}
	if(StrCompare(curVolLabel,volLabel) == 0) { found = true; break; }
    }
    MemPtrFree(volLabel);
    MemPtrFree(curVolLabel);
    if(!found) return errNone+1;
    err = VFSDirCreate(vRefn,"/fl");
    if(err != errNone && err != vfsErrFileAlreadyExists) return err;
    FileRef db;
    err = VFSFileOpen(vRefn, dbname,vfsModeRead,&db);
    if(err != errNone) return err;
    CharPtr scale;
    for(int i=0;i<10;i++)
    {
        scale = fileGetLine(db);
        if(scale == 0) { err = errNone+1; break; }
        scaleList(i,scale);
    }
    if(err != errNone) {
        for(int i=0;i<10;i++)
        {
             scale = scaleList(i); scaleList(i,0);
             if(scale != 0) MemPtrFree(scale);
        }
    }
    VFSFileClose(db);
    return errNone;
}

static void SetScaleLabels( FormPtr frmP )
{
  if(scaleList(0) == 0) return;
  FieldPtr fld = (FieldPtr)FrmGetObjectPtrFromID(frmP, mLab01 );
  setStr2Fld(fld,scaleList(0),60);
  fld = (FieldPtr)FrmGetObjectPtrFromID(frmP, mLab02 );
  setStr2Fld(fld,scaleList(1),60);
  fld = (FieldPtr)FrmGetObjectPtrFromID(frmP, mLab03 );
  setStr2Fld(fld,scaleList(2),60);
  fld = (FieldPtr)FrmGetObjectPtrFromID(frmP, mLab04 );
  setStr2Fld(fld,scaleList(3),60);
  fld = (FieldPtr)FrmGetObjectPtrFromID(frmP, mLab05 );
  setStr2Fld(fld,scaleList(4),60);
  fld = (FieldPtr)FrmGetObjectPtrFromID(frmP, mLab06 );
  setStr2Fld(fld,scaleList(5),60);
  fld = (FieldPtr)FrmGetObjectPtrFromID(frmP, mLab07 );
  setStr2Fld(fld,scaleList(6),60);
  fld = (FieldPtr)FrmGetObjectPtrFromID(frmP, mLab08 );
  setStr2Fld(fld,scaleList(7),60);
  fld = (FieldPtr)FrmGetObjectPtrFromID(frmP, mLab09 );
  setStr2Fld(fld,scaleList(8),60);
  fld = (FieldPtr)FrmGetObjectPtrFromID(frmP, mLab10 );
  setStr2Fld(fld,scaleList(9),60);
}

static void SaveData(FormPtr frmP) {
    Char *cDate = (Char*)MemPtrNew(12);
    Char *cTime = (Char*)MemPtrNew(6);
    Char *dat = (Char*)MemPtrNew(22);
    ControlType *lst = (ControlType *)FrmGetObjectPtrFromID(frmP,Scale);
    const Char *val = CtlGetLabel(lst);
    DateTimeType date;
    TimSecondsToDateTime(TimGetSeconds(),&date);
    DateTemplateToAscii("^4r-^3z-^0z ",date.month,date.day,date.year,cDate,12);
    TimeToAscii(date.hour,date.minute,tfColon24h,cTime);
    dat = StrCopy(dat,cDate);
    if(date.hour < 10) dat = StrCat(dat, "0");
    dat = StrCat(dat,cTime);
    dat = StrCat(dat,";");
    dat = StrCat(dat,val);
    dat = StrCat(dat,"\r\n");
    Err err = WriteData(dat);
    MemPtrFree(cDate);
    MemPtrFree(cTime);
    MemPtrFree(dat);
    switch(err) {
    case (errNone+1):
      FrmAlert (WrongCard);
      break;
    case errNone:
      FrmAlert (Stored);
      break;
    default:
      FrmAlert (StoreErr);
      break;
    }
}

static void NewDatacard() {
    Err err;
    FormPtr frmP = FrmGetActiveForm();
    Char *curVolLabel = (Char*)MemPtrNew(255);
    MemSet(curVolLabel,255,0);
    UInt16 vRefn;
    Boolean set = false;
    UInt32 vItr = vfsIteratorStart;
    VolumeInfoType vi;
    while(vItr != vfsIteratorStop)
    {
        err = VFSVolumeEnumerate(&vRefn,&vItr);
        if(err == expErrEnumerationEmpty) {
            MemPtrFree(curVolLabel);
            FrmAlert (InsCard);
            FrmUpdateForm( FrmGetFormId( frmP ), 0 );
            return;
        }
        VFSVolumeInfo(vRefn,&vi);
        if(vi.attributes & vfsVolumeAttrReadOnly) continue;
        VFSVolumeGetLabel(vRefn,curVolLabel,255);
        StoreLabel(curVolLabel);
        set = true;
        break;
    }
    MemPtrFree(curVolLabel);
    if(!set) FrmAlert (InsCard);
    else FrmAlert (CardSaved);
    FrmUpdateForm( FrmGetFormId( frmP ), 0 );
}

Boolean mainFrmEvtHnd(EventPtr event) {
  Boolean handled = false;
  FormPtr frmP = FrmGetActiveForm();

  switch (event->eType) {
    case ctlSelectEvent:
      switch(event->data.ctlSelect.controlID) {
        case mB01:
          SaveData(frmP);
          FrmUpdateForm( FrmGetFormId( frmP ), frmRedrawUpdateCode );
          handled = true;
          break;

        case mB02:
          NewDatacard();
          FrmUpdateForm( FrmGetFormId( frmP ), frmRedrawUpdateCode );
          handled = true;
          break;

        case mB03:
          FrmPopupForm(SCALE);
          handled = true;
          break;
      }
      break;

    case frmOpenEvent:
      FrmDrawForm ( frmP );
      SetScaleLabels( frmP );
      handled = true;
      break;

    case frmUpdateEvent:          // To do any custom drawing here, first call FrmDrawForm(),
      if(event->data.frmUpdate.updateCode == frmRedrawUpdateCode+1) SetScaleLabels( frmP );
      FrmDrawForm ( frmP );       // then do your drawing, and then set handled to true.
      handled = true;
      break;

    case frmCloseEvent:
      handled = true;
      break;

    default:
      break;
  }
  return handled;
}
