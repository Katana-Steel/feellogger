#include "main.h"
typedef Char* CharPtr;

Boolean mainFrmEvtHnd(EventPtr event);
Boolean scaleFrmEvtHnd(EventPtr event);
void setStr2Fld(FieldPtr dst,const CharPtr src, UInt16 maxLen=0);
Err GetVLabelFromADB(Char *lab);
void StoreLabel(const Char *lab);

extern CharPtr scale0;
extern CharPtr scale1;
extern CharPtr scale2;
extern CharPtr scale3;
extern CharPtr scale4;
extern CharPtr scale5;
extern CharPtr scale6;
extern CharPtr scale7;
extern CharPtr scale8;
extern CharPtr scale9;

CharPtr scaleList(int i);
void scaleList(int i, CharPtr scale);
