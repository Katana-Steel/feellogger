#include "test.h"
#include "mform.h"

void
setStr2Fld(FieldPtr dst,const CharPtr src, UInt16 maxLen)
{
  UInt16 len = StrLen(src), memSize=maxLen;
  if(maxLen != 0 && len > maxLen) len = maxLen;
  if(memSize == 0) memSize = 124;
  memSize += (memSize%4)+4;
  FldSetTextHandle(dst,NULL);
  MemHandle ftxt  = MemHandleNew(memSize);
  Char *fldT = (Char*)MemHandleLock(ftxt);
  MemSet(fldT,memSize,0);
  StrNCopy(fldT,src,len);
  MemHandleUnlock(ftxt);
  FldSetTextHandle(dst,ftxt);
  FldDrawField(dst);
}

void
fillFields( FormPtr frmP )
{
  if(scaleList(0) == 0) return;
  FieldPtr fld = (FieldPtr)FrmGetObjectPtrFromID(frmP, f01 );
  setStr2Fld(fld,scaleList(0),60);
  fld = (FieldPtr)FrmGetObjectPtrFromID(frmP, f02 );
  setStr2Fld(fld,scaleList(1),60);
  fld = (FieldPtr)FrmGetObjectPtrFromID(frmP, f03 );
  setStr2Fld(fld,scaleList(2),60);
  fld = (FieldPtr)FrmGetObjectPtrFromID(frmP, f04 );
  setStr2Fld(fld,scaleList(3),60);
  fld = (FieldPtr)FrmGetObjectPtrFromID(frmP, f05 );
  setStr2Fld(fld,scaleList(4),60);
  fld = (FieldPtr)FrmGetObjectPtrFromID(frmP, f06 );
  setStr2Fld(fld,scaleList(5),60);
  fld = (FieldPtr)FrmGetObjectPtrFromID(frmP, f07 );
  setStr2Fld(fld,scaleList(6),60);
  fld = (FieldPtr)FrmGetObjectPtrFromID(frmP, f08 );
  setStr2Fld(fld,scaleList(7),60);
  fld = (FieldPtr)FrmGetObjectPtrFromID(frmP, f09 );
  setStr2Fld(fld,scaleList(8),60);
  fld = (FieldPtr)FrmGetObjectPtrFromID(frmP, f10 );
  setStr2Fld(fld,scaleList(9),60);
}

CharPtr
setFld2Str(CharPtr dst, FieldPtr src)
{
    MemHandle hnd = FldGetTextHandle(src);
    CharPtr txt = (CharPtr)MemHandleLock(hnd);
    if(txt == NULL) return dst;
    UInt16 len = StrLen(txt);
    dst = (CharPtr)MemPtrNew(len+(len%4)+4);
    MemSet(dst,len+(len%4)+4,0);
    StrNCopy(dst,txt,len);
    MemHandleUnlock(hnd);
    return dst;
}

void WriteScaleList(void)
{
    Err err;
    Boolean getLabel = false;
    Boolean found = false;
    static const Char *dbname = "/fl/scale.fdb";
    Char *volLabel = (Char*)MemPtrNew(255);
    MemSet(volLabel,255,0);
    Char *curVolLabel = (Char*)MemPtrNew(255);
    MemSet(curVolLabel,255,0);
    err = GetVLabelFromADB(volLabel);
    if(err != errNone) getLabel = true;
    VolumeInfoType vi;
    UInt16 vRefn;
    UInt32 vItr = vfsIteratorStart;
    while(vItr != vfsIteratorStop)
    {
	err = VFSVolumeEnumerate(&vRefn,&vItr);
	if(err != errNone) {
	    MemPtrFree(volLabel);
	    MemPtrFree(curVolLabel);
	    return;
	}
        VFSVolumeInfo(vRefn,&vi);
        if(vi.attributes & vfsVolumeAttrReadOnly) continue;
	VFSVolumeGetLabel(vRefn,curVolLabel,255);
	if(getLabel) {
	    StoreLabel(curVolLabel);
	    StrCopy(volLabel, curVolLabel);
	}
	if(StrCompare(curVolLabel,volLabel) == 0) { found = true; break; }
    }
    MemPtrFree(volLabel);
    MemPtrFree(curVolLabel);
    if(!found) return;
    err = VFSDirCreate(vRefn,"/fl");
    if(err != errNone && err != vfsErrFileAlreadyExists) return;
    FileRef db;
    err = VFSFileOpen(vRefn, dbname,vfsModeWrite|vfsModeTruncate,&db);
    if(err != errNone) return;
    for(int i= 0;i<10;i++)
    {
//        if(scaleList[i] == 0) continue;
        err = VFSFileWrite(db,StrLen(scaleList(i)),scaleList(i),NULL);
        err = VFSFileWrite(db,1,"\n",NULL);
    }
    VFSFileClose(db);
}

void StoreScale( FormPtr frmP )
{
  FieldPtr fld = (FieldPtr)FrmGetObjectPtrFromID(frmP, f01 );
  if(fld == NULL) return;
  CharPtr scale;
  if(FldGetTextPtr(fld) != NULL) {
    scale = scaleList(0);
    if(scale != 0) MemPtrFree(scale);
    scale=setFld2Str(scale,fld);
    scaleList(0,scale);
  }
  fld = (FieldPtr)FrmGetObjectPtrFromID(frmP, f02 );
  if(fld == NULL) return;
  if(FldGetTextPtr(fld) != NULL) {
    scale = scaleList(1);
    if(scale != 0) MemPtrFree(scale);
    scale=setFld2Str(scale,fld);
    scaleList(1,scale);
  }
  fld = (FieldPtr)FrmGetObjectPtrFromID(frmP, f03 );
  if(fld == NULL) return;
  if(FldGetTextPtr(fld) != NULL) {
    scale = scaleList(2);
    if(scale != 0) MemPtrFree(scale);
    scale=setFld2Str(scale,fld);
    scaleList(2,scale);
  }
  fld = (FieldPtr)FrmGetObjectPtrFromID(frmP, f04 );
  if(fld == NULL) return;
  if(FldGetTextPtr(fld) != NULL) {
    scale = scaleList(3);
    if(scale != 0) MemPtrFree(scale);
    scale=setFld2Str(scale,fld);
    scaleList(3,scale);
  }
  fld = (FieldPtr)FrmGetObjectPtrFromID(frmP, f05 );
  if(fld == NULL) return;
  if(FldGetTextPtr(fld) != NULL) {
    scale = scaleList(4);
    if(scale != 0) MemPtrFree(scale);
    scale=setFld2Str(scale,fld);
    scaleList(4,scale);
  }
  fld = (FieldPtr)FrmGetObjectPtrFromID(frmP, f06 );
  if(fld == NULL) return;
  if(FldGetTextPtr(fld) != NULL) {
    scale = scaleList(5);
    if(scale != 0) MemPtrFree(scale);
    scale=setFld2Str(scale,fld);
    scaleList(5,scale);
  }
  fld = (FieldPtr)FrmGetObjectPtrFromID(frmP, f07 );
  if(fld == NULL) return;
  if(FldGetTextPtr(fld) != NULL) {
    scale = scaleList(6);
    if(scale != 0) MemPtrFree(scale);
    scale=setFld2Str(scale,fld);
    scaleList(6,scale);
  }
  fld = (FieldPtr)FrmGetObjectPtrFromID(frmP, f08 );
  if(fld == NULL) return;
  if(FldGetTextPtr(fld) != NULL) {
    scale = scaleList(7);
    if(scale != 0) MemPtrFree(scale);
    scale=setFld2Str(scale,fld);
    scaleList(7,scale);
  }
  fld = (FieldPtr)FrmGetObjectPtrFromID(frmP, f09 );
  if(fld == NULL) return;
  if(FldGetTextPtr(fld) != NULL) {
    scale = scaleList(8);
    if(scale != 0) MemPtrFree(scale);
    scale=setFld2Str(scale,fld);
    scaleList(8,scale);
  }
  fld = (FieldPtr)FrmGetObjectPtrFromID(frmP, f10 );
  if(fld == NULL) return;
  if(FldGetTextPtr(fld) != NULL) {
    scale = scaleList(9);
    if(scale != 0) MemPtrFree(scale);
    scale=setFld2Str(scale,fld);
    scaleList(9,scale);
  }
  WriteScaleList();
}

Boolean
scaleFrmEvtHnd(EventPtr event)
{
  Boolean handled = false;
  FormPtr frmP = FrmGetActiveForm();

  switch (event->eType) {
    case ctlSelectEvent:
      EventType evt = *event;
      switch(event->data.ctlSelect.controlID) {
        case sB01:
          StoreScale(frmP);
          evt.eType = frmCloseEvent;
          FrmDispatchEvent(&evt);
          FrmUpdateForm( MAIN, frmRedrawUpdateCode+1 );
          handled = true;
          break;

        case sB02:
          evt.eType = frmCloseEvent;
          FrmDispatchEvent(&evt);
          handled = true;
          break;

      }
      break;

    case frmOpenEvent:
      FrmDrawForm ( frmP );
      fillFields( frmP );
      handled = true;
      break;

    case frmUpdateEvent:          // To do any custom drawing here, first call FrmDrawForm(),
      FrmDrawForm ( frmP );       // then do your drawing, and then set handled to true.
      handled = true;
      break;

    case frmCloseEvent:
      FrmReturnToForm(0);
      handled = true;
      break;

    default:
      break;
  }
  return handled;
}

